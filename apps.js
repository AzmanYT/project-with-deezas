document.addEventListener('alpine:init', () => {
    Alpine.data('produk', () => ({
        items : [
            {id: 1, name: 'Bakso Polos', img : 'bakso_polos.jpg', price : 15000, desc : 'Berbentuk bulat dan terbuat dari daging sapi pilihan dengan tekstur lembut dan kenyal'},
            {id: 2, name: 'Bakso Urat', img : 'bakso_urat.jpg', price : 15000, desc : 'Bakso Polos dengan Urat sapi yang menggiurkan'},
            {id: 3, name: 'Bakso Telur', img : 'bakso_telur.jpg', price : 15000, desc : 'Bakso Polos dan bakso berisi telur ayam menggugah selera'},
            {id: 4, name: 'Bakso Tahu', img : 'bakso_tahu.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 5, name: 'Mie Ayam Bakso', img : 'mie_ayam_bakso.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 6, name: 'Mie Ayam', img : 'mie_ayam_biasa.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 7, name: 'Mie Ayam Urat', img : 'mie_ayam_urat.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 8, name: 'Siomay Kuah', img : 'siomay_kuah.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 9, name: 'Siomay Tahu', img : 'siomay_tahu.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
        ]
    }))

    Alpine.store('cart', {
        items : [],
        total : 0,
        quantity : 0,

        add(newItem) {

            const cartItem = this.items.find((item)=>item.id == newItem.id);
            if(!cartItem){
                            this.items.push({...newItem, quantity:1, total: newItem.price})
                            this.quantity++
                            this.total += newItem.price
            }else{
                this.items = this.items.map((item) => {
                  
                  if(item.id != newItem.id){
                      return item;
                  }else{
                    item.quantity++
                    item.total = item.price * item.quantity;
                    this.quantity++
                    this.total += item.price
                    return item
                  }
                })
            }
           console.log(this.total)
        },

        remove(item) {
const cartItem = this.items.find((i) => i.id == item.id);
            if(cartItem.quantity > 1){
                cartItem.quantity--;
                cartItem.total -= cartItem.price;
                this.quantity--;
                this.total -= cartItem.price;
            }else{
                this.items = this.items.filter((i) => i.id != item.id);
                this.quantity--;
                this.total -= cartItem.price;
            }
        }
    })
})

document.addEventListener('alpine:init', () => {
    Alpine.data('minuman', () => ({
        items : [
            {id: 22, name: 'Aneka Pop Ice', img : 'aneka_popice.jpg', price : 15000, desc : 'Berbentuk bulat dan terbuat dari daging sapi pilihan dengan tekstur lembut dan kenyal'},
            {id: 23, name: 'Cingcau', img : 'cingcau.jpg', price : 15000, desc : 'Bakso Polos dengan Urat sapi yang menggiurkan'},
            {id: 24, name: 'Es Jeruk', img : 'esjeruk.jpg', price : 15000, desc : 'Bakso Polos dan bakso berisi telur ayam menggugah selera'},
            {id: 25, name: 'Es Teh', img : 'esteh.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 26, name: 'Jeruk Panas', img : 'jerukpanas.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 27, name: 'Jus Alpukat', img : 'jus_alpukat.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 28, name: 'Jus Apel', img : 'jus_apel.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 29, name: 'Jus Belimbing', img : 'jus_belimbing.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 30, name: 'Jus Buah Naga', img : 'jus_buahNaga.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 10, name: 'Jus Jambu', img : 'jus_jambu.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 11, name: 'Jus Mangga', img : 'jus_mangga.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 12, name: 'Jus Melon', img : 'jus_melon.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 13, name: 'Jus Sirsak', img : 'jus_sirsak.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 14, name: 'Jus Stroberi', img : 'jus_stroberi.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 15, name: 'Teh Hangat', img : 'tehanget.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
        ]
    }))

    Alpine.store('cart', {
        items : [],
        total : 0,
        quantity : 0,

        add(newItem) {

            const cartItem = this.items.find((item)=>item.id == newItem.id);
            if(!cartItem){
                            this.items.push({...newItem, quantity:1, total: newItem.price})
                            this.quantity++
                            this.total += newItem.price
            }else{
                this.items = this.items.map((item) => {
                  
                  if(item.id != newItem.id){
                      return item;
                  }else{
                    item.quantity++
                    item.total = item.price * item.quantity;
                    this.quantity++
                    this.total += item.price
                    return item
                  }
                })
            }
           console.log(this.total)
        },

        remove(item) {
const cartItem = this.items.find((i) => i.id == item.id);
            if(cartItem.quantity > 1){
                cartItem.quantity--;
                cartItem.total -= cartItem.price;
                this.quantity--;
                this.total -= cartItem.price;
            }else{
                this.items = this.items.filter((i) => i.id != item.id);
                this.quantity--;
                this.total -= cartItem.price;
            }
        }
    })
})


document.addEventListener('alpine:init', () => {
    Alpine.data('snack', () => ({
        items : [
            {id: 99, name: 'Jasuke', img : 'jasuke.jpg', price : 15000, desc : 'Berbentuk bulat dan terbuat dari daging sapi pilihan dengan tekstur lembut dan kenyal'},
            {id: 92, name: 'Kerupuk Kulit', img : 'kerupuk_kulit.jpg', price : 15000, desc : 'Bakso Polos dengan Urat sapi yang menggiurkan'},
            {id: 91, name: 'Krupuk', img : 'krupuk.jpg', price : 15000, desc : 'Bakso Polos dan bakso berisi telur ayam menggugah selera'},
            {id: 98, name: 'Pangsit', img : 'pangsit.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            {id: 91, name: 'Peyek', img : 'peyek_kacang.jpg', price : 15000, desc : 'Bakso dengan tahu coklat yang dipadukan dengan bumbu kaldu ayam lezat'},
            ]
    }))

    Alpine.store('cart', {
        items : [],
        total : 0,
        quantity : 0,

        add(newItem) {

            const cartItem = this.items.find((item)=>item.id == newItem.id);
            if(!cartItem){
                            this.items.push({...newItem, quantity:1, total: newItem.price})
                            this.quantity++
                            this.total += newItem.price
            }else{
                this.items = this.items.map((item) => {
                  
                  if(item.id != newItem.id){
                      return item;
                  }else{
                    item.quantity++
                    item.total = item.price * item.quantity;
                    this.quantity++
                    this.total += item.price
                    return item
                  }
                })
            }
           console.log(this.total)
        },

        remove(item) {
const cartItem = this.items.find((i) => i.id == item.id);
            if(cartItem.quantity > 1){
                cartItem.quantity--;
                cartItem.total -= cartItem.price;
                this.quantity--;
                this.total -= cartItem.price;
            }else{
                this.items = this.items.filter((i) => i.id != item.id);
                this.quantity--;
                this.total -= cartItem.price;
            }
        }
    })
})


const rupiah = (number) => {
    return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits : 0, 
    }).format(number);
}